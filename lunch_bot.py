#!/usr/bin/env python
# -*- coding: utf-8 -*-


from datetime import datetime
from json import dump
from json import load
import logging
import os
import sys
import re
from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater


import bot_messages as bm
import config



# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

logger = logging.getLogger(__name__)

fn = sys.argv[0]
fn = re.sub('\/lunch_bot\.py$', '',fn)

def start(bot, update):
    """Send a message when the command /start is issued."""
    bot.send_message(chat_id=update.message.chat_id, text=bm.START)


def help(bot, update):
    """Send a message when the command /help is issued."""
    bot.send_message(chat_id=update.message.chat_id, text=bm.HELP)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)

def almoco(bot, update):
    t = fn + "/script/tmp/almoco"
    with open(t,"rb") as f:
        lines = f.read()
        bot.send_message(chat_id=update.message.chat_id, text=lines)

def janta(bot, update):
    t = fn + "/script/tmp/jantar"
    with open(t,"rb") as f:
        lines = f.read()
        bot.send_message(chat_id=update.message.chat_id, text=lines)

def cafe(bot, update):
    t = fn + "/script/tmp/coffe"
    with open(t,"rb") as f:
        lines = f.read()
        bot.send_message(chat_id=update.message.chat_id, text=lines)
            

def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.    
    updater = Updater(config.TOKEN)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('help', help))
    dp.add_handler(CommandHandler('almoco',almoco))
    dp.add_handler(CommandHandler('cafe',cafe))
    dp.add_handler(CommandHandler('janta',janta))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()


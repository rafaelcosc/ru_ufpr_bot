# Bot telegram RU UFPR

Bot que fornece o cafe/almoço/janta do dia atual do RU-UFPR.

## Getting Started

Execute lunch_bot.py, e requisite a informação ao bot com os comandos definidos em bot_messages.py.

O arquivo bash (./script/script_bot.sh) deve ser executado 1 vez por dia para recolher as informações no site, recomendo utilizar crontab ou criar um service para executar essa tarefa. Caso seja do interesse também existe um script para recolher as informações do RU-central (./script/fds_script_bot.sh).


### Prerequisites

```
xidel

Python

```

### Installing

```
Após clonar o projeto , modifique o arquivo config.py.example para config.py e adicione as informações TOKEN (a string que o botFather envia após a criação do bot) e ADMINS (seu nome do telegram).

```

## Contact

* **Rafael Dias** - rafaelcosc@gmail.com
